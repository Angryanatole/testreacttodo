const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: "./src/index.tsx",
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: "[name].bundle.js"
  },
  devtool: "source-map",
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx', '.styl'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader",
      },
      {
        enforce: "pre",
        test: /\.js$/, loader: "source-map-loader",
        query: {
          presets: ['env', 'react']
        }
      },
      {
        test: /\.styl$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          "css-loader",
          "stylus-loader"
        ]
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.(jpe?g|svg|png|ttf|woff2?|otf)(\?.+)?$/,
        use: {
          loader: 'file-loader',
          options: {}
        }
      },
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
  ]
};
