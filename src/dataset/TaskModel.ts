export interface TaskModel {
  id: string;
  text: string;
  date: Date;
  isDone: boolean;
}
