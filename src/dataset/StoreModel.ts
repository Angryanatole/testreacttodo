import { TaskModel } from './TaskModel';

export interface StoreModel {
  tasks: TaskModel[];
}
