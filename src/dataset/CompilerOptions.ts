export interface CompilerOptions {
  compiler: string;
  framework: string;
}
