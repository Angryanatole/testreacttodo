import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import './styles.styl';
import MainScreen from './components/main/main';
import rootReducer from './reducers/rootReducer';

const store = createStore(rootReducer);

ReactDOM.render(
  <Provider store={store}>
    <MainScreen compiler="TypeScript" framework="React"/>
  </Provider>,
  document.getElementById('root'),
);
