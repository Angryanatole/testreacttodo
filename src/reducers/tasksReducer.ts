import { ADD_TASK, REMOVE_TASK, UPDATE_TASK } from '../constants/tasks';
import { AddTaskModel, RemoveTaskModel, UpdateTaskModel } from '../actions/tasksActions';
import v4 = require('uuid/v4');
import { TaskModel } from '../dataset/TaskModel';

export const defaultState: TaskModel[] = [
  {
    id: v4(),
    text: 'Подготовить отчеты',
    date: new Date(),
    isDone: true,
  },
  {
    id: v4(),
    text: 'Погладить Пушистика',
    date: new Date(),
    isDone: false,
  },
  {
    id: v4(),
    text: '6',
    date: new Date(),
    isDone: true,
  },
];

const tasks = ((state = defaultState, action: any) => {
  switch (action.type) {
    case ADD_TASK:
      const { task } = action as AddTaskModel;
      return [
        ...state,
        task,
      ];
    case REMOVE_TASK:
      const { id } = action as RemoveTaskModel;
      return state.filter((task: TaskModel) => task.id !== id);
    case UPDATE_TASK:
      const updatedTask = (action as UpdateTaskModel).task;
      return state.map(task => task.id === updatedTask.id ? updatedTask : task);
    default:
      return state;
  }
});

export default tasks;
