import { ADD_TASK, REMOVE_TASK, UPDATE_TASK } from '../constants/tasks';
import { TaskModel } from '../dataset/TaskModel';

export interface AddTaskModel {
  type: string;
  task: TaskModel;
}

export interface RemoveTaskModel {
  type: string;
  id: string;
}

export interface UpdateTaskModel {
  type: string;
  task: TaskModel;
}

export const addTask = (task: TaskModel): AddTaskModel => ({
  type: ADD_TASK,
  task,
});

export const removeTask = (id: string): RemoveTaskModel => ({
  type: REMOVE_TASK,
  id,
});

export const updateTask = (task: TaskModel): UpdateTaskModel => ({
  type: UPDATE_TASK,
  task,
});
