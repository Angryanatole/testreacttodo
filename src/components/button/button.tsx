import * as React from 'react';
import './button.styl';

export interface ButtonProps {
  text: string;
  onClick?: Function;
  hasRemove?: boolean;
  type?: string;
}

export class Button extends React.Component<ButtonProps> {
  constructor(props: ButtonProps) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }
  onClick() {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }
  render() {
    const { text, hasRemove, type } = this.props;
    const style = hasRemove ? 'button remove' : 'button';
    return (
      <button className={style}
              onClick={this.onClick}
              type={type}
      >
        {hasRemove &&
          <i className="remove">
            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="18" viewBox="0 0 14 18">
              <path fill="#566394" fillRule="evenodd"
                d="M1 16c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V4H1v12zm2.46-7.12l1.41-1.41L7 9.59l2.12-2.12 1.41 1.41L8.41 11l2.12 2.12-1.41 1.41L7 12.41l-2.12 2.12-1.41-1.41L5.59 11 3.46 8.88zM10.5 1l-1-1h-5l-1 1H0v2h14V1h-3.5z"/>
            </svg>
          </i>
        }
        {text}
      </button>
    );
  }
}
