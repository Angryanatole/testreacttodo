import * as React from 'react';
import './task.styl';
import { Button } from '../button/button';
import { TaskModel } from '../../dataset/TaskModel';
import Checkbox from '../checkbox/checkbox';
import { format } from 'date-fns';

interface TaskProps extends TaskModel {
  onDelete: Function;
  onToggle: Function;
  onTextUpdate: (text: string) => void;
}

export class Task extends React.Component<TaskProps, {editMode: boolean, text: string}> {
  constructor(props: TaskProps) {
    super(props);
    this.state = {
      editMode: false,
      text: '',
    };
    this.onCheckboxClick = this.onCheckboxClick.bind(this);
    this.openEditMode = this.openEditMode.bind(this);
    this.closeEditMode = this.closeEditMode.bind(this);
  }
  onCheckboxClick(event: React.MouseEvent): void {
    this.props.onToggle();
  }
  openEditMode() {
    const { isDone, text } = this.props;

    if (this.state.editMode || isDone) {
      return;
    }
    this.setState({
      editMode: true,
      text,
    });
  }
  closeEditMode() {
    this.props.onTextUpdate(this.state.text);
    this.setState({
      editMode: false,
      text: '',
    });
  }
  onTextChanged(newText: string) {
    this.setState({
      text: newText,
    });
  }
  onFormSubmit(e: React.SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();
    this.closeEditMode();
  }
  render() {
    const { text, isDone, date } = this.props;
    const { editMode } = this.state;
    return (
      <div className="task row vertical-center" onDoubleClick={this.openEditMode}>
        <div className="task-content">
          <Checkbox checked={isDone}
                    onCheckboxClick={e => this.onCheckboxClick(e)}
          />
          {editMode ?
            <form onSubmit={(e: React.SyntheticEvent<HTMLFormElement>) => this.onFormSubmit(e)}>
              <input className="input"
                     type="text"
                     onBlur={this.closeEditMode}
                     defaultValue={text}
                     onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.onTextChanged(e.target.value)}
                     autoFocus/>
            </form> :
            <label className="task-content__text" >{text}</label>
          }
          {!isDone &&
            <label className="task-content__date">
              {format(date, 'DD MMMM YYYY', { locale: 'ru-Ru' })}
            </label>
          }
        </div>
        {isDone &&
          <div className="task-content__button-container">
            <Button text="Удалить" onClick={this.props.onDelete} hasRemove/>
          </div>
        }
      </div>
    );
  }
}
