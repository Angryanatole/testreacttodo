import * as React from 'react';
import './checkbox.styl';

export interface CheckboxProps {
  checked: boolean;
  onCheckboxClick?: (event: React.MouseEvent) => void;
}

export default class Checkbox extends React.Component<CheckboxProps> {
  render() {
    const style = this.props.checked ? 'checkbox checkbox-checked' : 'checkbox';
    return (
      <div className={style}
           onClick={this.props.onCheckboxClick}>
      </div>
    );
  }
}
