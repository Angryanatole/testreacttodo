import * as React from 'react';
import './taskList.styl';
import { TaskModel } from '../../dataset/TaskModel';
import { Task } from '../task/task';
import { removeTask, RemoveTaskModel,
  updateTask, UpdateTaskModel } from '../../actions/tasksActions';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

export interface TaskListModel {
  tasks: TaskModel[];
  removeTask: ((id: string) => RemoveTaskModel);
  updateTask: ((task: TaskModel) => UpdateTaskModel);
}

const tasksSortFunc = (a: TaskModel, b: TaskModel) =>
  b.date.getTime() - a.date.getTime();

class TaskList extends React.Component<TaskListModel> {
  deleteTask(id: string) {
    this.props.removeTask(id);
  }
  toggleTask(task: TaskModel) {
    this.props.updateTask({ ...task, isDone: !task.isDone });
  }
  updateTaskText(task: TaskModel, text: string) {
    this.props.updateTask({
      ...task,
      text,
    });
  }
  render() {
    const notFinishedTasks = this.props.tasks
      .filter(task => !task.isDone)
      .sort(tasksSortFunc);
    const finishedTasks = this.props.tasks
      .filter(task => task.isDone);
    return (
      <div className="task-list">
        <div className="task-list__tasks">
          {notFinishedTasks.map(task =>
              <Task {...task}
                    key={task.id}
                    onDelete={() => this.deleteTask(task.id)}
                    onToggle={() => this.toggleTask(task)}
                    onTextUpdate={newText => this.updateTaskText(task, newText)}
              />,
            )
          }
        </div>
        {notFinishedTasks.length > 0 && finishedTasks.length > 0 &&
          <div className="divider"></div>
        }
        <div className="task-list__tasks">
          {finishedTasks.map(task =>
              <Task {...task}
                    key={task.id}
                    onDelete={() => this.deleteTask(task.id)}
                    onToggle={() => this.toggleTask(task)}
                    onTextUpdate={newText => this.updateTaskText(task, newText)}
              />,
            )
          }
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    removeTask: (id: string) => dispatch(removeTask(id)),
    updateTask: (task: TaskModel) => dispatch(updateTask(task)),
  };
};

export default connect(null, mapDispatchToProps)(TaskList);
