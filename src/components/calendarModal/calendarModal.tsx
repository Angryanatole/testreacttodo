import * as React from 'react';
import './calendarModal.styl';
import { addDays, addMonths, endOfMonth, format,
  getDaysInMonth, startOfMonth, subDays, subMonths } from 'date-fns';

export interface CalendarModalProps {
  show: boolean;
  onClose: (date: Date) => void;
}

export interface CalendarModalState {
  startDate: Date;
}

export interface Day {
  date: Date;
  type: 'notThisMonth' | 'thisMonth' | 'current';
}

export default class CalendarModal extends React.Component<CalendarModalProps, CalendarModalState> {
  constructor(props: CalendarModalProps) {
    super(props);
    this.state = {
      startDate: new Date(),
    };
  }
  getWeekdayTitles(): string[] {
    return ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
  }
  chooseDate(e: React.MouseEvent<HTMLDivElement>, day: Day) {
    e.stopPropagation();
    this.props.onClose(day.date);
  }
  showPreviousMonth(e: React.MouseEvent<SVGSVGElement>) {
    e.stopPropagation();
    const newDate = subMonths(this.state.startDate, 1);
    this.setState({
      startDate: newDate,
    });
  }
  showNextMonth(e: React.MouseEvent<SVGSVGElement>) {
    e.stopPropagation();
    const newDate = addMonths(this.state.startDate, 1);
    this.setState({
      startDate: newDate,
    });
  }
  areDatesEqual(date1: Date, date2: Date): boolean {
    return date1.getDate() === date2.getDate() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getFullYear() === date2.getFullYear();
  }
  getDays(): Day[] {
    const today = new Date();
    const startDate = this.state.startDate;
    const firstDayInMonth = startOfMonth(startDate);
    const lastDayInMonth = endOfMonth(startDate);
    const totalDaysInThisMonth = getDaysInMonth(startDate);

    const res: Day[] = [];

    for (let i = 0; i < totalDaysInThisMonth; i++) {
      const newDate = addDays(firstDayInMonth, i);
      res.push({
        date: newDate,
        type: this.areDatesEqual(newDate, today) ?
          'current' :
          'thisMonth',
      });
    }

    const numberOfDaysBefore = firstDayInMonth.getDay() === 0 ?
      6 : firstDayInMonth.getDay() - 1;
    const beforeRes: Day[] = [];

    for (let i = numberOfDaysBefore; i > 0; i--) {
      beforeRes.push({
        date: subDays(firstDayInMonth, i),
        type: 'notThisMonth',
      });
    }

    const lastWeekdayOfMonth = lastDayInMonth.getDay();
    const numberOfDaysAfter = 7 - lastWeekdayOfMonth;
    const afterRes: Day[] = [];
    for (let i = 1; i <= numberOfDaysAfter; i++) {
      afterRes.push({
        date: addDays(lastDayInMonth, i),
        type: 'notThisMonth',
      });
    }

    return [...beforeRes, ...res, ...afterRes];
  }

  render() {
    const { show } = this.props;
    return(
      <div className={show ? 'calendarModal' : 'calendarModal-hidden'}>
        <div className="calendarModal__content">
          <div className="calendarModal__header">
            {this.getWeekdayTitles().map((day: string, i: number) => (
              <span className="calendarModal__header__weekday" key={i}>
                {day}
              </span>
            ))}
          </div>
          <div className="calendarModal__months">
            <svg onClick={(e: React.MouseEvent<SVGSVGElement>) => this.showPreviousMonth(e)}
                 xmlns="http://www.w3.org/2000/svg" width="8" height="13" viewBox="0 0 8 13">
              <path fill="#2C5286" fillRule="evenodd" d="M7.41 11.09L2.83 6.5l4.58-4.59L6 .5l-6 6 6 6z"/>
            </svg>
            <span className="calendarModal__months__month">
              {format(this.state.startDate, 'MMMM YYYY')}
            </span>
            <svg onClick={(e: React.MouseEvent<SVGSVGElement>) => this.showNextMonth(e)}
                 xmlns="http://www.w3.org/2000/svg" width="8" height="13" viewBox="0 0 8 13">
              <path fill="#2C5286" fillRule="evenodd" d="M.59 1.91L5.17 6.5.59 11.09 2 12.5l6-6-6-6z"/>
            </svg>
          </div>
          <div className="calendarModal__days">
            {this.getDays().map((day: Day, i: number) => {
              let style;
              switch (day.type) {
                case 'notThisMonth':
                  style = 'calendarModal__days__day calendarModal__days__day-extra';
                  break;
                case 'thisMonth':
                  style = 'calendarModal__days__day';
                  break;
                default:
                  style = 'calendarModal__days__day calendarModal__days__day-current';
                  break;
              }
              return (
                <div className={style}
                     onClick={(e: React.MouseEvent<HTMLDivElement>) => this.chooseDate(e, day)}
                     key={i}
                >
                  {day.date.getDate()}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
