import * as React from 'react';
import { CompilerOptions } from '../../dataset/CompilerOptions';
import './main.styl';
import { Button } from '../button/button';
import TaskList from '../taskList/taskList';
import { StoreModel } from '../../dataset/StoreModel';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { addTask, AddTaskModel } from '../../actions/tasksActions';
import v4 = require('uuid/v4');
import { TaskModel } from '../../dataset/TaskModel';
import { format } from 'date-fns';
import CalendarModal from '../calendarModal/calendarModal';

interface MainScreenProps extends CompilerOptions {
  tasks: TaskModel[];
  addTask: ((task: TaskModel) => AddTaskModel);
}

interface MainScreenState {
  text: string;
  validate: boolean;
  showCalendarModal: boolean;
  chosenDate: Date;
}

class MainScreen extends React.Component<MainScreenProps, MainScreenState> {

  taskTextRef: React.RefObject<HTMLInputElement> = React.createRef();

  constructor(props: MainScreenProps) {
    super(props);
    this.state = {
      text: '',
      validate: false,
      showCalendarModal: false,
      chosenDate: new Date(),
    };
    this.addNewTask = this.addNewTask.bind(this);
    this.openModal = this.openModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
  }

  updateText(str: string) {
    this.setState({
      text: str,
      validate: false,
    });
  }

  addNewTask(e: React.SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();
    const { text, chosenDate } = this.state;
    if (text.length > 0) {
      this.props.addTask({
        id: v4(),
        isDone: false,
        date: chosenDate,
        text,
      });
      this.setState({
        text: '',
        validate: false,
      });
    } else {
      this.setState({
        validate: true,
      });
    }
    this.taskTextRef.current.focus();
  }

  openModal() {
    this.setState({
      showCalendarModal: true,
    });
  }

  onCloseModal(date: Date) {
    if (!date) {
      return;
    }
    this.setState({
      showCalendarModal: false,
      chosenDate: date,
    });
  }

  render() {
    const { text, validate, showCalendarModal, chosenDate } = this.state;
    const inputContainerStyle = this.state.validate ?
      'add-task-container add-task-container-error' :
      'add-task-container';
    const inputStyle = this.state.validate ?
      'add-task-container__input add-task-container__input-error' :
      'add-task-container__input';
    return (
      <div className="main">
        <h1>Дела</h1>
        <form onSubmit={(e: React.SyntheticEvent<HTMLFormElement>) => this.addNewTask(e)}>
          <div className="row">
            <div className={inputContainerStyle}>
              <input value={text}
                     onChange={e => this.updateText(e.target.value)}
                     ref={this.taskTextRef}
                     className={inputStyle}
                     type="text"
                     placeholder={validate ? 'Ошибка. Введите текст.' : 'Новое дело'}/>
              <label onClick={this.openModal} className="add-task-container__date">
                {format(chosenDate, 'DD MMMM YYYY')}
              </label>
              <svg onClick={this.openModal} xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20">
                <path fill="#566394" fillRule="evenodd" d="M16 2h-1V0h-2v2H5V0H3v2H2C.89 2 .01 2.9.01 4L0 18a2 2 0 0 0 2 2h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm0 16H2V7h14v11zM4 9h5v5H4V9z"/>
              </svg>
            </div>
            <Button text="Добавить" type="submit" />
          </div>
        </form>
        <div className="divider"></div>
        <TaskList tasks={this.props.tasks}/>
        <CalendarModal show={showCalendarModal}
                       onClose={date => this.onCloseModal(date)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: StoreModel) => {
  return {
    tasks: state.tasks,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    addTask: (task: TaskModel) => dispatch(addTask(task)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
