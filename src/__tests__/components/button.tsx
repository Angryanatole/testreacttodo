import { configure, shallow } from 'enzyme';
import { Button } from '../../components/button/button';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';

configure({
  adapter: new Adapter(),
});

describe('Button', () => {
  it('should render without throwing an error', () => {
    expect(shallow(<Button text="fjsdkfj"/>)
      .find('button.button').length)
      .toBe(1);
  });
});
