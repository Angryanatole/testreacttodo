import { configure, shallow } from 'enzyme';
import * as React from 'react';
import CalendarModal from '../../components/calendarModal/calendarModal';
import * as Adapter from 'enzyme-adapter-react-16';

configure({
  adapter: new Adapter(),
});

const component = shallow(<CalendarModal onClose={() => {}} show/>);
const instance: CalendarModal = component.instance() as CalendarModal;

describe('Calendar', () => {
  it('should render without throwing an error', () => {
    expect(component.find('div.calendarModal').length)
      .toBe(1);
  });
  it('should give 7 weekdays', () => {
    expect(instance.getWeekdayTitles().length)
      .toBe(7);
  });
  it('should correctly compare dates (should be equal)', () => {
    expect(instance.areDatesEqual(new Date('2001-12-13'), new Date('2001-12-13')))
      .toBe(true);
  });
  it('should correctly compare dates (date error)', () => {
    expect(instance.areDatesEqual(new Date('2001-12-17'), new Date('2001-12-13')))
      .toBe(false);
  });
  it('should correctly compare dates (month error)', () => {
    expect(instance.areDatesEqual(new Date('2001-11-13'), new Date('2001-12-13')))
      .toBe(false);
  });
  it('should correctly compare dates (year error)', () => {
    expect(instance.areDatesEqual(new Date('2003-12-13'), new Date('2001-12-13')))
      .toBe(false);
  });
});
