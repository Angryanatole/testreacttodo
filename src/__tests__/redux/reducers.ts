import { ADD_TASK, REMOVE_TASK } from '../../constants/tasks';
import { AddTaskModel, RemoveTaskModel } from '../../actions/tasksActions';
import tasks, { defaultState } from '../../reducers/tasksReducer';
import { TaskModel } from '../../dataset/TaskModel';

const testAddTask: TaskModel = {
  id: 'id',
  text: 'fdsjkfls',
  date: new Date(),
  isDone: false,
};

describe('task Reducer', () => {
  it('ADD_TASK success', () => {
    const state: TaskModel[] = [];
    const action: AddTaskModel = {
      type: ADD_TASK,
      task: testAddTask,
    };
    const result = tasks(state, action);
    expect(result).toEqual([testAddTask]);
  });
  it('REMOVE_TASK success', () => {
    const state: TaskModel[] = defaultState;
    const idToRemove = state[0].id;
    const action: RemoveTaskModel = {
      type: REMOVE_TASK,
      id: idToRemove,
    };
    const result = tasks(state, action);
    expect(result).toEqual(defaultState.filter(task => task.id !== idToRemove));
  });
});
